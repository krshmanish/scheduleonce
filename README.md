To run this setup, you have to install
    1. NPM
    2. Angular CLI.

Then go to the scheduleOnce folder and run 'ng server'.
Navigate to `http://localhost:4200/`.

All the logic written in 'src/app/app.component.ts' file.


Steps to find the answers 

To find the first question answer you need to pass the two value using input text. First value is start value and second value will be end value. Then you have to click on find Output button to show the output.

Second question answer already displayed in page.

For third question answer you have to click on 'Get invoice details' button and then open a alert box.
Please read alert message carefully.



# ScheduleOnce

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
