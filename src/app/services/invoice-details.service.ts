import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class InvoiceDetailsService {
  constructor(private httpService: HttpClient) {}

  getDetails(url: any) {
    return this.httpService.get(url, { responseType: 'text' as 'json' });
  }
}
