import { Component } from '@angular/core';
import { InvoiceDetailsService } from './services/invoice-details.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  macroPolo: string;
  numbers: any;

  isCompleted: boolean;

  node = {
    name: 'root',
    children: [
      {
        name: 'a',
        children: [
          {
            name: 'x',
            children: [
              {
                name: 'xx',
                children: []
              }
            ]
          },
          {
            name: 'y',
            children: [
              {
                name: 'yy',
                children: []
              }
            ]
          }
        ]
      },
      { name: 'b', children: [] },
      {
        name: 'c',
        children: [
          { name: 'd', children: [] },
          { name: 'e', children: [] },
          { name: 'f', children: [] }
        ]
      }
    ]
  };

  constructor(private invoiceService: InvoiceDetailsService) {
    this.numbers = {};
    this.isCompleted = false;
  }

  getNumberSeries() {
    this.invoiceService
      .getDetails('../../assets/files/number.txt')
      .subscribe(res => {
        this.numbers = this.getNumbers(res);
      });
  }

  getNumbers(data) {
    const numbers = this.getNumberObject();
    const numberArray = data.split('\n');
    for (const array of numberArray) {
      let objKey = 0;
      let n = 0;
      for (let i = 0; i < array.length - 1; i++) {
        if (n === 3) {
          objKey++;
          n = 0;
        }
        n++;
        numbers[objKey] = numbers[objKey] + array[i];
      }
    }
    return numbers;
  }

  onAnswerClick(answer, start, end) {
    switch (answer) {
      case 'FIRST':
        this.macroPolo = '';
        for (let i = start; i <= end; i++) {
          if (i % 4 === 0) {
            this.macroPolo = this.macroPolo + 'Macro';
          }

          if (i % 7 === 0) {
            this.macroPolo = this.macroPolo + 'Polo';
          }

          if (this.macroPolo[this.macroPolo.length - 1] !== 'o') {
            this.macroPolo = this.macroPolo + i;
          }

          if (i !== parseInt(end, 10)) {
            this.macroPolo = this.macroPolo + ', ';
          }
        }
        break;
      case 'THIRD':
        this.invoiceService
          .getDetails('../../assets/files/invoice1.txt')
          .subscribe(res => {
            this.getDetails(res);
          });
        break;
      default:
        break;
    }
  }

  getDetails(data) {
    const array = data.split('\n');
    let numberStr = '';
    for (const item of array) {
      if (item.length > 2) {
        numberStr = numberStr + item + '\n';
      } else {
        if (numberStr) {
          console.log(numberStr);
          const numbers = this.getNumbers(numberStr);
          const originalNumber = this.generateNumbers(numbers);
          console.log(originalNumber);
        }
        numberStr = '';
      }
    }
    alert(
      'To see the output, you need to open the console by using f12 key or right click on the window and select inspect option'
    );
  }

  generateNumbers(newNumber) {
    const storedNumber = this.numbers;
    let originalNumber = '';
    for (const newKey of Object.keys(newNumber)) {
      let isAvailable = false;
      for (const sotredKey of Object.keys(storedNumber)) {
        if (newNumber[newKey] === storedNumber[sotredKey]) {
          originalNumber = originalNumber + sotredKey;
          isAvailable = true;
          break;
        }
      }
      if (!isAvailable && newNumber[newKey]) {
        originalNumber = originalNumber + '?';
      }
    }
    return originalNumber;
  }

  getNumberObject() {
    return {
      1: '',
      2: '',
      3: '',
      4: '',
      5: '',
      6: '',
      7: '',
      8: '',
      9: '',
      0: ''
    };
  }
}
